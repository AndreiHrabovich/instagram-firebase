//
//  AppDelegate.swift
//  Instagram-Firebase
//
//  Created by A23 on 11/12/19.
//  Copyright © 2019 A23. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = SignUpViewController(nibName: "SignUpView", bundle: nil)
        window?.makeKeyAndVisible()
        
        return true
    }

}

