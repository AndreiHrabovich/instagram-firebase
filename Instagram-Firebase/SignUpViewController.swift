//
//  ViewController.swift
//  Instagram-Firebase
//
//  Created by A23 on 11/12/19.
//  Copyright © 2019 A23. All rights reserved.
//

import UIKit
import FirebaseAuth

final class SignUpViewController: UIViewController {
    
    @IBOutlet private weak var plusPhotoButton: UIButton!
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var signUpButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addInputFieldListeners()
    }
    
    @IBAction private func handleSignUp(_ sender: UIButton) {
        guard let email = emailTextField.text, email.count > 0,
            let username = usernameTextField.text, username.count > 0,
            let password = passwordTextField.text, password.count > 0 else { return }
        
        Auth.auth().createUser(withEmail: email, password: password) { (authDataResult: AuthDataResult?, error: Error?) in
            if let error = error {
                print("Failed to create a user", error)
            }
            
            guard let authDataResult = authDataResult else {return}
            print("Successfully created use with user id:", authDataResult.user.uid)
        }
    }
    
    private func addInputFieldListeners() {
        emailTextField.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        usernameTextField.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
    }
    
    @objc private func handleTextInputChange() {
        let isFormValid = emailTextField.text?.count ?? 0 > 0
            && usernameTextField.text?.count ?? 0 > 0
            && passwordTextField.text?.count ?? 0 > 0
        
        if isFormValid {
            signUpButton.backgroundColor = .systemBlue
            signUpButton.isEnabled = true
        } else {
            signUpButton.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
            signUpButton.isEnabled = false
        }
    }
    
    
}

